require 'spec_helper'
require 'its'       # to kiedyś było w rspecu, ale teraz juz nie ma, trzeba dodatkowy gem doczepiać
require 'date'


describe Swieto do
  let(:dzień_nieświęto)         {Swieto.new(Date.new(2015,8,11))}
  let(:dzień_bozeNarodzenie1)   {Swieto.new(Date.new(2015,12,25))}
  let(:dzień_wielkanoc1)        {Swieto.new(Date.new(2015,4,5))}
  let(:dzień_zieloneSwiatki)    {Swieto.new(Date.new(2015,5,24))}
  let(:dzień_bozeCialo)         {Swieto.new(Date.new(2015,6,4))}

  describe "#świętoPaństwowe?" do
    it "Dzień nie będący świętem" do
      dzień_nieświęto.świętoPaństwowe?.should be false
    end
    it "Boże Narodzenie" do
      dzień_bozeNarodzenie1.świętoPaństwowe?.should eq("pierwszy dzień Bożego Narodzenia")
    end
    it "Wielkanoc" do
      dzień_wielkanoc1.świętoPaństwowe?.should eq("pierwszy dzień Wielkiej Nocy"  )
    end
    it "Zielone świątki" do
      dzień_zieloneSwiatki.świętoPaństwowe?.should eq("pierwszy dzień Zielonych Świątek"  )
    end
    it "Boże Ciało" do
      dzień_bozeCialo.świętoPaństwowe?.should eq("dzień Bożego Ciała"  )
    end
  end
end
