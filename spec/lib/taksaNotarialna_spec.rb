# coding: utf-8
require 'spec_helper'
require 'date'

describe TaksaNotarialna::TaksaKwotowa do
  let(:taksa)     {TaksaNotarialna::TaksaKwotowa.new()}

  it "Bez daty jest datowany dzisiejszą datą" do
    taksa.stanPrawny == Date.today
  end

  describe "Taksa kwotowa 100%" do
    it "nienajwyższa" do
      expect(taksa.pełnaStawka(70000)).to eq(1050)
    end
    it "przesadna" do
      expect(taksa.pełnaStawka(2500000)).to eq(8020)
    end
    it "przesadna z 1 grupą" do
      expect(taksa.pełnaStawka(2500000, true)).to eq(7500)
    end
    it "przesadnaxx" do
      expect(taksa.pełnaStawka(25000000)).to eq(10000)
    end
  end
end
