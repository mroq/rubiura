# coding: utf-8
require 'spec_helper'
require 'date'


describe PodatekSD::PodatekWGrupach do
  let(:podatek)     {PodatekSD::PodatekWGrupach.new()}

  it "Bez daty jest datowany dzisiejszą datą" do
    podatek.stanPrawny == Date.today
  end

  it "Podatek w grupie drugiej" do
    expect(podatek.kwota('II', 200000)).to eq(22305)
  end
end
    
  
