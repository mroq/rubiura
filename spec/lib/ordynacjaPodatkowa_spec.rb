# coding: utf-8
require 'spec_helper'
require 'its'       # to kiedyś było w rspecu, ale teraz juz nie ma, trzeba dodatkowy gem doczepiać
require 'date'

describe OrdynacjaPodatkowa::OrdynacjaPodatkowa do

  let(:stanPrawny) {Date.new(2015,8,11)}
  let(:ustawaDzis) {OrdynacjaPodatkowa::OrdynacjaPodatkowa.new(Date.new(2015,8,11))}

  describe "#initialize z danymi" do
    subject { ustawaDzis }
    its (:stanPrawny) {should == stanPrawny}
    its (:publikator) {should == "Dz.U.2015.613"}
  end

  let(:dzień_święto) {Date.new(2015,12,25)}
  let(:dzień_wielkanoc1) {Date.new(2015,4,5)}
  let(:dzień_pracujacy) {Date.new(2015,8,11)}
  let(:dzień_niedziela) {Date.new(2015,8,16)}
  let(:dzień_sobota) {Date.new(2015,8,1)}

  describe "#terminyDzieńWyłączonyZUpływu?" do
    context "Dzień jest świętem pierwszego dnia Bożego Narodzenia" do
      subject {ustawaDzis.terminyDzieńWyłączonyZUpływu?(dzień_święto)}
      it {should be true}
    end
    context "Dzień jest świętem pierwszego dnia Wielkanocy" do
      subject {ustawaDzis.terminyDzieńWyłączonyZUpływu?(dzień_wielkanoc1)}
      it {should be true}
    end
    context "Dzień jest pracujący" do
      subject {ustawaDzis.terminyDzieńWyłączonyZUpływu?(dzień_pracujacy)}
      it {should be false}
    end
    context "Dzień jest niedzielą" do
      subject {ustawaDzis.terminyDzieńWyłączonyZUpływu?(dzień_niedziela)}
      it {should be true}
    end
    context "Dzień jest sobotą" do
      subject {ustawaDzis.terminyDzieńWyłączonyZUpływu?(dzień_niedziela)}
      it {should be true}
    end
  end

  let(:ustawaBezDaty) {OrdynacjaPodatkowa::OrdynacjaPodatkowa.new()}
  describe "#initialize bez danych" do
    subject { ustawaBezDaty }
    its (:stanPrawny) {should == Date.today}
    its (:publikator) {should eq "Dz.U.2015.613"}
  end

  let(:ustawaDawno) {OrdynacjaPodatkowa::OrdynacjaPodatkowa.new(Date.new(2000,8,11))}
  describe "bardzo stary #initialize" do
    subject { ustawaDawno }
    its (:publikator) {should eq "Dz.U.1997.137.926"}
  end

  describe "#initialize" do
    it "zbyt stara data" do
      expect {OrdynacjaPodatkowa::OrdynacjaPodatkowa.new(Date.new(1995,8,11))}.to raise_error(NotImplementedError)
    end
  end

end

describe OrdynacjaPodatkowa::Termin do
  let(:termin0)  {OrdynacjaPodatkowa::Termin.new(Date.new(2015,8,1), 0, :dzień)}
  let(:termin1)  {OrdynacjaPodatkowa::Termin.new(Date.new(2015,8,1), 4, :dzień)}
  let(:termin1a) {OrdynacjaPodatkowa::Termin.new(Date.new(2015,8,11), 4, :dzień)}
  let(:termin1b) {OrdynacjaPodatkowa::Termin.new(Date.new(2015,12,20), 5, :dzień)}
  let(:termin2)  {OrdynacjaPodatkowa::Termin.new(Date.new(2015,2,11), 6, :miesiąc)}
  let(:termin2a) {OrdynacjaPodatkowa::Termin.new(Date.new(2015,1,31), 1, :miesiąc)}
  let(:termin2b) {OrdynacjaPodatkowa::Termin.new(Date.new(2015,3,31), 1, :miesiąc)}
  let(:termin3)  {OrdynacjaPodatkowa::Termin.new(Date.new(2015,8,11), 2, :rok)}
  let(:termin3a)  {OrdynacjaPodatkowa::Termin.new(Date.new(2012,2,29), 1, :rok)}
  describe "#initialize" do
    subject { termin1 }
    its (:dzieńPoczątkowy) {should == Date.new(2015,8,1) }
    its (:długość) {should == 4}
    its (:jednostka) {should == :dzień}
    its (:dzieńKońcowy) {should == Date.new(2015,8,5) }
    it "Koniec terminu wypadający w zielone swiatki" do
      termin1a.dzieńKońcowy.should eq(Date.new(2015,8,17))
    end
    it "Koniec terminu wypadający w boże narodzenie" do
      termin1b.dzieńKońcowy.should eq(Date.new(2015,12,28))
    end
    it "termin 6 miesiecy" do
      termin2.dzieńKońcowy.should eq(Date.new(2015,8,11))
    end
    it "termin 31 w lutym z przesunieciem na najblizszy dzien roboczy" do
      termin2a.dzieńKońcowy.should eq(Date.new(2015,3,2))
    end
    it "termin 31 w kwietniu" do
      termin2b.dzieńKońcowy.should eq(Date.new(2015,4,30))
    end
    it "termin 2 lat" do
      termin3.dzieńKońcowy.should eq(Date.new(2017,8,11))
    end
    it "termin roku z przesunieciem do tylu" do
      termin3a.dzieńKońcowy.should eq(Date.new(2013,2,28))
    end
  end
end

describe "Kalkulator odsetek" do
  let(:kalk)       {OrdynacjaPodatkowa::KalkulatorOdsetek.new()}
  let(:terminZap2) {Date.new(2013,1,25)}
  let(:dataWpl2)   {Date.new(2013,12,10)}
  let(:kw2)        {15000}
  let(:kw3)        {15000000}
  it "#liczOdsetki", "Kazus 11" do
    kalk.liczOdsetki(terminZap2, dataWpl2, kw2).should eq 1411
  end
  it "#liczOdsetki", "jeden dzień" do
    kalk.liczOdsetki(Date.new(2005,1,20), Date.new(2005,1,21), kw3).should eq 6575.30
  end

  let(:terminZap1) {Date.new(2014,1,4)}
  let(:dataWpl1)   {Date.new(2014,1,11)}
  let(:kw1)        {100}
  it "#liczOdsetki", "Powinien wyzerować poniżej progu zaokrąglenia" do
    kalk.liczOdsetki(terminZap1, dataWpl1, kw1).should eq 0
  end
end
