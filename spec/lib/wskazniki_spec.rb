# coding: utf-8
describe Wskazniki::StopaLombardowa do
  let(:stopa) {Wskazniki::StopaLombardowa}
  it "powinna wskazywać właściwą kwotę w dacie 2015-4-4" do
    stopa.stopaNaDzień(Date.new(2015,4,4)).should eq 2.5
  end
  it "powinna wskazywać właściwą kwotę w dacie 2014-1-4" do
    stopa.stopaNaDzień(Date.new(2014,1,4)).should eq 4.0
  end
end
