# coding: utf-8
require 'spec_helper'
require 'date'

describe PodatekCC::PodatekStawkaPodstawowa do
  let(:podatek)     {PodatekCC::PodatekStawkaPodstawowa.new()}

  it "Bez daty jest datowany dzisiejszą datą" do
    podatek.stanPrawny == Date.today
  end

  describe "Podatek na zasadach ogolnych" do
    it "od umowy spółki" do
      expect(podatek.kwota('Art. 7 ust. 1 pkt 9', 6000)).to eq(30)
    end
  end
end
