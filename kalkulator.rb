# coding: utf-8



require 'tk'

root = TkRoot.new { title "Kalkulator" }

TkLabel.new(root) do
  text 'Kalkulator odsetek od zaległości podatkowych'
  pack {
    padx 15
    pady 15
    side 'left'
  }
end

terminZapłaty = TkEntry.new(root) {
  place(
    'height' => 25,
    'width'  => 150,
    'x'      => 10,
    'y'      => 40)
}

def termin
  puts terminZapłaty.get
  exit
end

bLicz = TkButton.new(root) {
  text "licz"
  borderwidth 5
  underline 0
  state "normal"
  cursor "watch"
  font TkFont.new('times 12 bold')
  foreground "red"
  activebackground "blue"
  relief "groove"
  command (proc {termin})
  pack {
    side 'right'
    padx 50
    pady 10
  }
}
    


Tk.mainloop
