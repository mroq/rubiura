# coding: utf-8
require_relative 'lib/ordynacjaPodatkowa'
require 'date'

puts "Data początkowa:"
dp = gets.chomp
puts "Data końcowa:"
dk = gets.chomp
puts "Kwota:"
kw = gets.chomp.to_f

def datify (d)
  d = d.split('-').map {|n| n.to_i}
  Date.new(d[0], d[1], d[2])
end

dk = datify dk
dp = datify dp

k = OrdynacjaPodatkowa::KalkulatorOdsetek.new()
puts k.liczOdsetki(dp, dk, kw).to_f
