#!/bin/env ruby
# coding: utf-8

def podatekSD
  require_relative 'lib/podatekSD'
  puts "Podatek od spadków i darowizn:"

  puts "Rodzaj:
[1] - w grupach
[2] - zasiedzenie"
  ans = gets.chomp.to_i
    
  case ans
  when 1
    puts "Grupa [1|2|3]:"
    g = {1 => 'I', 2 => 'II', 3 => 'III'}[gets.chomp.to_i]
    puts "Podstawa:"
    p = gets.chomp.to_i
    puts PodatekSD::PodatekWGrupach.new().kwota(g,p)
  when 2
    puts "Podstawa:"
    p = gets.chomp.to_i
    puts PodatekSD::PodatekOdZasiedzenia.new().kwota(p)
  else
    raise "Nie ma takiego rodzaju PSD"
  end
end

def podatekCC
  require_relative 'lib/podatekCC'
  puts "Podatek od czynności cywilnoprawnych"
  pod = PodatekCC::PodatekStawkaPodstawowa.new()
  lista = {}
  pod.stawki.each_index {|ind| lista[ind] = pod.stawki[ind]}
  puts "Która stawka?"
  lista.keys.each {|x| puts "#{x} - #{lista[x]}"}

  ans = gets.chomp.to_i
  if lista.keys.member?(ans)
    puts "Podstawa:"
    podst = gets.chomp.to_i
    puts pod.kwota(lista[ans], podst)
  else
    puts "Nie ma takiej stawki"
  end
end
 
        ans = nil
        while ans != 'q'
          
          puts "
Który podatek?:
[1] - PSD
[2] - PCC
"
          case (gets.chomp.to_i)
          when 1
            podatekSD
          when 2
            podatekCC
          else
            false
          end
        end
