#!/usr/bin/env ruby -w
#
# coding: utf-8
require_relative 'lib/ordynacjaPodatkowa'
require 'date'


puts "Data początkowa:"
d = gets.chomp
d= d.split('-').map {|n| n.to_i}
d= Date.new(d[0], d[1], d[2])

puts "ile?:"
i = gets.chomp.to_i

puts <<H
czego?:
1) dni
2) miesiecy
3) lat
H
n = {'1' => :dzień, '2' => :miesiąc, '3' => :rok}[gets.chomp]

puts "Termin końcowy dla daty " + d.to_s + " to: " + OrdynacjaPodatkowa::Termin.new(d, i, n  ).dzieńKońcowy.to_s
