# coding: utf-8
module Wskazniki
  require 'bigdecimal'
  require 'date'
  require 'rexml/document'

  class StopaProcentowa
  end

  class NBP < StopaProcentowa
  end

  class StopaLombardowa < NBP
    include REXML

    def self.tabela
      self.wypełnijTabele
      @Tabela.clone
    end

    def self.stopaNaDzień(dzień)
      self.wypełnijTabele
      self.stopaNaDzień_static(dzień)
    end
    
    def self.stopaNaDzień_static(dzień)
      self.wypełnijTabele unless @Tabela
      @Tabela.reverse.each { |x|
        return x[1] if dzień >= x[0]
      }
      raise "brak danych dla daty ".concat(dzień.to_s)
    end

    def self.pokaz
      self.wypełnijTabele
      @Tabela.each {|x| printf("%s    %f\n", x[0], x[1].to_f)}
    end
    
    private
    def self.wypełnijTabele
      @Tabela = []
      xml = Document.new(File.new File.expand_path("../wskazniki-NBP.xml", __FILE__))
      xml.root.elements.each("stopaLombardowa/stopa") { |s| @Tabela << [s.elements["data"].text, s.elements["procent"].text] }
      @Tabela.each { |x|
        x[0] = x[0].split("-").map {|y| y.to_i}
        x[0] = Date.new(x[0][0], x[0][1], x[0][2])
        x[1] = BigDecimal.new(x[1])
      }
      @Tabela = @Tabela.sort { |l,r|
        l[0] <=> r[0]
      }
    end
  end
end
