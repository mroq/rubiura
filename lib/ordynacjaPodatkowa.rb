# coding: utf-8
module OrdynacjaPodatkowa
  require 'bigdecimal'
  require 'date'

  ##########################################################
  PUBS = [
    [Date.new(2015, 5, 6),  "Dz.U.2015.613"],
    [Date.new(2012, 7, 3),  "Dz.U.2012.749"],
    [Date.new(2005, 1, 14), "Dz.U.2005.8.60"],
    [Date.new(1998, 1, 1),  "Dz.U.1997.137.926"],
  ]

  ##########################################################
  ZmianyMetodyLiczeniaStawkiOdsetek = {}  #art. 56 ordynacji
  ZmianyMetodyLiczeniaStawkiOdsetek[Date.new(2001,6,5)] = lambda do
    |dzień|
    stopa = Wskazniki::StopaLombardowa.stopaNaDzień_static(dzień) * 2 / 100
    {metoda: lambda {|kwota| kwota * (stopa / 365)},
     stopaOdsetek: stopa}
  end
  ZmianyMetodyLiczeniaStawkiOdsetek[Date.new(2010,11,9)] = lambda do
    |dzień|
    stopa = (Wskazniki::StopaLombardowa.stopaNaDzień_static(dzień) * 2 + 2)
    stopa = BigDecimal.new(8) if stopa < 8
    stopa = stopa / 100
    {metoda: lambda {|kwota| kwota * (stopa / 365)},
     stopaOdsetek: stopa}
  end
  ##########################################################
  ZmianyMetodyZaokrąglaniaOdsetek = {}
  ZmianyMetodyZaokrąglaniaOdsetek[Date.new(2003,1,1)] = lambda {|x| x.round(1)} # § 13 ust. 2 Dz.U.2002.240.2063 (pełne dziesiątki)
  ZmianyMetodyZaokrąglaniaOdsetek[Date.new(2006,1,1)] = lambda {|x| x.round()} # art 64 Ordynacji podatkowej + § 15 Dz.U.2005.165.1373
  ##########################################################

  class Publikator
    def self.publikator(dzień=Date.today)
      PUBS.each { |x| if dzień > x[0] then return x[1] end }
      raise ArgumentError.new("Data sprzed wejścia w życie ustawy")
    end
  end

  class OrdynacjaPodatkowa; 
    require_relative 'swieta'
    require_relative 'wskazniki'


    attr_reader   :stanPrawny,
                  :publikator

    def initialize(dataStanuPrawnego=nil)
      dataStanuPrawnego = Date.today unless dataStanuPrawnego
      @stanPrawny = dataStanuPrawnego
      begin
        @publikator = Publikator.publikator(stanPrawny)
      rescue ArgumentError
        raise NotImplementedError.new("zbyt stara data dla Ordunacji podatkowej. Powinien przekazywać dalej do poprzedniego aktu, ale jeszcze tego nie robi")
      end
    end

    def zaokrąglanie(kwota)
      stawkaZaZwłokęMetodaLiczenia()[1].call(kwota)
    end

    def terminyDzieńWyłączonyZUpływu?(dzień)      # to do poprawki gruntownej
      return true if
      Swieto.new(dzień).świętoPaństwowe? or
        dzień.saturday?
      false
    end

    def stawkaZaZwłokęMetodaLiczenia(data=nil)  # dupa, dupa
      "[metodaLiczenia, metodaZaokraglania]"
      data = stanPrawny unless data

      [ZmianyMetodyLiczeniaStawkiOdsetek, ZmianyMetodyZaokrąglaniaOdsetek].map { # [hash, hash] -> [lambda, lambda]
        |zmiany|
        daty = zmiany.keys
        daty.sort!.reverse!
        akt = nil
        daty.each {
          |d|
          if d <= data
            akt = zmiany[d]
            break
          end
        }
        raise "Zbyt stara data" unless akt
        akt
      }
      
      # lambda {|x| funkcje[1].call(funkcje[0].call(x))}
    end
  end

  ###################################################################################
  class Termin
    attr_reader :dzieńPoczątkowy,
      :długość,
      :jednostka,
      :akt
    def initialize(data, ilość, jednostka)
      @dzieńPoczątkowy = data
      @długość = ilość
      @jednostka = jednostka
      @akt = OrdynacjaPodatkowa.new(dzieńPoczątkowy)
    end

    def dzieńKońcowy
      dzieńk = nil
      case jednostka
      when :dzień
        dzieńk = dodajDni
      when :miesiąc
        dzieńk = dodajMiesiące
      when :rok
        dzieńk = dodajLata
      else
        raise ArgumentError
      end

      while akt.terminyDzieńWyłączonyZUpływu?(dzieńk) do      # szukaj nastepnego pracujacego
        dzieńk = dzieńk.next
      end
      dzieńk
    end

    private
    def dodajDni(ile=nil, dzień=nil)
      ile = długość unless ile
      dzień = dzieńPoczątkowy unless dzień
      if ile == 0 then dzień else dodajDni(ile-1, dzień.next) end
    end

    def dodajMiesiące(ile=nil, dzień=nil)
      ile = długość unless ile
      dzień = dzieńPoczątkowy unless dzień

      return dzień if ile == 0

      y = dzień.year
      m = dzień.month
      d = dzień.day

      if (m + ile) > 12
        y = y + 1
        m = m + ile - 12
      else
        m = m + ile
      end

      poprawnaBądźNiższaDataRoczna(y,m,d)
    end

    def dodajLata(ile=nil, dzień=nil)
      ile = długość unless ile
      dzień = dzieńPoczątkowy unless dzień
      poprawnaBądźNiższaDataRoczna(dzień.year+ile, dzień.month, dzień.day)
    end

    def poprawnaBądźNiższaDataRoczna(y,m,d)
      begin
        Date.new(y,m,d)
      rescue ArgumentError
        poprawnaBądźNiższaDataRoczna(y,m,d-1)
      end
    end
  end

  class KalkulatorOdsetek

    def initialize
      @ord = OrdynacjaPodatkowa.new()
      @tdni = [] # widok ostatniego obliczenia
      @metody = nil
    end

    def liczOdsetki(termin, wpłata, kwota, terminWliczony: false)
      @tdni = []
      metodyLiczenia = @ord.stawkaZaZwłokęMetodaLiczenia(termin)  # tu jest wołanie startowej metody liczenia (nie wiem po co w sumie)
      until termin == wpłata
        termin = termin.next if :terminWliczony
        metodyLiczenia = @ord.stawkaZaZwłokęMetodaLiczenia(termin)  # tu dla każdego dnia uzyskuję lambde metody liczenia
                                                                    # obowiązującą w danym dniu <- NADPISUJE poprzednią
        metodaDnia = metodyLiczenia[0].call(termin) # następnie dopiero na podstawie tej metody lczenia uzyskuję dane
                                                    # do liczenia według tej metody na konkretny dzień... akurat tutaj
                                                    # będzie to ta sama data... Nie wiem czy to nei jest zbyt podupcone
        @tdni << [termin.to_s,
                  sprintf("%.2f", metodaDnia[:stopaOdsetek].to_f),
                  metodaDnia[:metoda].call(kwota)  # bigdecimal wysokości odsetek na dzień
                 ]
        # p tdni
      end
      sumaOdsetek = @tdni.inject(BigDecimal.new(0)) {|suma, dzień| suma + dzień[2]}
      metodyLiczenia[1].call(sumaOdsetek)           # tu wołam ostatnią metodę zaokrąglania - powinien wracać bigdecimal
    end
  end
end
