# coding: utf-8

module PodatekCC
  class Podatek             # ta klasa powinna być gdzieś wyżej?
    require_relative 'ordynacjaPodatkowa'
    attr_reader :stanPrawny
    
    def initialize(dzień = nil)
      @stanPrawny =  if dzień then dzień else Date.today end
    end
  end

  class PodatekStawkaPodstawowa < Podatek
    Algi = {
          'Art. 7 ust. 1 pkt 1 lit. a'  => lambda {|x| x * 0.02},   # sprzedaż rzeczy
          'Art. 7 ust. 1 pkt 1 lit. b'  => lambda {|x| x * 0.01},   # sprzedaż innych praw
          'Art. 7 ust. 1 pkt 2 lit. a'  => lambda {|x| x * 0.02},   # zamiana lokali
          'Art. 7 ust. 1 pkt 2 lit. b'  => lambda {|x| x * 0.01},   # zamiana innych praw
          'Art. 7 ust. 1 pkt 4'         => lambda {|x| x * 0.02},   # pożyczka
          'Art. 7 ust. 1 pkt 7 lit. a'  => lambda {|x| x * 0.001},  # hipoteka na istniejącej
          'Art. 7 ust. 1 pkt 7 lit. b'  => lambda {19},             # hipoteka na nieustalonej
          'Art. 7 ust. 1 pkt 9'         => lambda {|x| x * 0.005},  # umowa spółki 0.5%
    }

    def stawki
      Algi.keys.sort
    end
        
    def algorytm(stawka)
      Algi.member?(stawka) ? Algi[stawka] : nil
    end

    def kwota(stawka, podstawa)
      ile = algorytm(stawka).call(podstawa)
      OrdynacjaPodatkowa::OrdynacjaPodatkowa.new(stanPrawny).zaokrąglanie(ile)
    end
  end
  
end
