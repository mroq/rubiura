# coding: utf-8
require 'date'



module PodatekSD

  class Podatek
    require_relative 'ordynacjaPodatkowa'
    attr_reader :stanPrawny
    
    def initialize(dzień = nil)
      @stanPrawny =  if dzień then dzień else Date.today end
    end
  end

  class PodatekWGrupach < Podatek
    def obniżenieOKwotęWolną(grupa, podstawa)
      case (grupa)
      when 'I'
        podstawa - 9637
      when 'II'
        podstawa - 7276
      when 'III'
        podstawa - 4902
      else
        podstawa
      end
    end

    def kwota(grupa, podstawa)
      grupy = {
        'I' =>      [[0,     0,       3],
                     [10278, 208.30,  5],
                     [20556, 822.20,  7]],
        'II' =>     [[0,     0,       7],
                     [10278, 719.50,  9],
                     [20556, 1644.50, 12]],
        'III' =>    [[0,     0,       12],
                     [10278, 1233.40, 16],
                     [20556, 2877.90, 20]],
      }
      raise ArgumentError unless grupy.member?(grupa)

      podstawa = obniżenieOKwotęWolną grupa, podstawa
      
      grupy[grupa].reverse.each {
        |skala|
      return OrdynacjaPodatkowa::OrdynacjaPodatkowa.new(stanPrawny).zaokrąglanie(skala[1] + ((skala[2] / 100.0) * (podstawa - skala[0]))) if podstawa > skala[0]
      }
    end
  end

  class PodatekOdZasiedzenia < Podatek
    def kwota(podstawa)
      OrdynacjaPodatkowa::OrdynacjaPodatkowa.new(stanPrawny).zaokrąglanie(0.07 * podstawa)  # nie ma kwoty wolnej wcale?
    end
  end
end
