# coding: utf-8

module TaksaNotarialna
  class Taksa             # ta klasa powinna być gdzieś wyżej?
    attr_reader :stanPrawny
    
    def initialize(dzień = nil)
      @stanPrawny =  if dzień then dzień else Date.today end
    end
  end

  class TaksaKwotowa < Taksa
    Inf = 1.0/0
    Algi = [
      [3000,    lambda {100}],
      [10000,   lambda {|x| 100 +  (0.03 *   (x - 3000))}],
      [30000,   lambda {|x| 310 +  (0.02 *   (x - 10000))}],
      [60000,   lambda {|x| 710 +  (0.01 *   (x - 30000))}],
      [1000000, lambda {|x| 1010 + (0.004 *  (x - 60000))}],
      [2000000, lambda {|x| 4770 + (0.002 *  (x - 1000000))}],
      [Inf,     lambda {|x, podatnik1grupa|
        ans = 6770 + (0.0025 * (x - 2000000))
        if podatnik1grupa
          ans > 7500 ? 7500 : ans
        else
          ans > 10000 ? 10000 : ans
        end
       }],
    ]

    def pełnaStawka(podstawa, podatnik1grupa = false)
      Algi.each {
        |alg|
        if podstawa <= alg[0]
          p alg[1].parameters.length
          case alg[1].parameters.length
          when 2
            case alg[1].parameters[1][1]
            when :podatnik1grupa
              return alg[1].call(podstawa, podatnik1grupa)
            else
              raise ArgumentError # tak ma być?
            end
          else
            return alg[1].call(podstawa)
          end
        end
      }
    end
  end
end
