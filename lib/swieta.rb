require 'date'

class Swieto
  attr_reader :data,
              :państwowe

  PAŃSTWOWE_STAŁE = {
    'Nowy rok'                              => [1,1],
    'Święto Trzech Króli'                   => [1,6],
    'Święto Państwowe Pierwszego Maja'      => [5,1],
    'Święto Narodowe Trzeciego Maja'        => [5,3],
    'Wniebowzięcie Najświętszej Maryi Panny'=> [8,15],
    'Wszystkich Świętych'                   => [11,1],
    'Narodowe Święto Niepodległości'        => [11,11],
    'pierwszy dzień Bożego Narodzenia'      => [12,25],
    'drugi dzień Bożego Narodzenia'         => [12,26]
  }

  def initialize(data)
    @data = data
    @państwowe = {}
    PAŃSTWOWE_STAŁE.keys.each {
      |x| @państwowe[x] = Date.new(data.year,
                                   PAŃSTWOWE_STAŁE[x][0],
                                   PAŃSTWOWE_STAŁE[x][1])
    }
    świętaRuchome
  end

  def świętoPaństwowe?
    found = false
    found = 'Niedziela' if data.sunday?
    państwowe.each {
      |s|
      found = s[0] if data == s[1]
    }
    return found
  end

  private
  def wielkanoc1dzien
    a = data.year % 19
    b = data.year / 100
    c = data.year % 100
    d = b / 4
    e = b % 4
    f = (b+8) / 25
    g = (b - f + 1) / 3
    h = (19 * a + b - d - g + 15) % 30
    i = c / 4
    k = c % 4
    l = (32 + 2 * e + 2 * i - h - k) % 7
    m = (a + 11 * h + 22 * l) / 451
    p = (h + l - 7 * m + 114) % 31
    dzień = p + 1;
    miesiąc =(h + l - 7 * m + 114) / 31
    [miesiąc, dzień]
  end

  def świętaRuchome
    @państwowe['pierwszy dzień Wielkiej Nocy'] = Date.new(data.year, wielkanoc1dzien[0], wielkanoc1dzien[1])

    @państwowe['drugi dzień Wielkiej Nocy'] = państwowe['pierwszy dzień Wielkiej Nocy'].next

    @państwowe['pierwszy dzień Zielonych Świątek'] = państwowe['drugi dzień Wielkiej Nocy'].next # 49 dni po wielkanocy
    47.times {@państwowe['pierwszy dzień Zielonych Świątek'] = państwowe['pierwszy dzień Zielonych Świątek'].next }

    @państwowe['dzień Bożego Ciała'] = państwowe['pierwszy dzień Zielonych Świątek'].next   # 60 dni po wielkanocy
    10.times {@państwowe['dzień Bożego Ciała'] = państwowe['dzień Bożego Ciała'].next}
  end
end
